import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SensorViewComponent } from './sensor-view/sensor-view.component';
import { AppComponent } from './app.component';


const routes: Routes = [
  { 
    path: 'hello',
    component: AppComponent
  },
  {
    path: '',
    component: SensorViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
