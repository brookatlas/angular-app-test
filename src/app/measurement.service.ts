import { Injectable } from '@angular/core';
import { measurement} from 'src/measurement';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MeasurementService {

  private client: HttpClient;
  constructor(
    private http: HttpClient
  ) 
  {
    this.client = http;
  }

  getMeasurements(): Observable<Object> {
    let influxUrl = "http://137.135.91.127:8086/query?pretty=true&db=sensor&q=SELECT * FROM machine GROUP BY * ORDER BY DESC LIMIT 1";
    let values = "";
    let jsonOutput = this.client.get(influxUrl);
    return jsonOutput;
  }

  measurementsToPoints(measurements: Array<measurement>): Array<Object> 
  {
    let coordArray = Array<Object>();
    measurements.forEach(measurement => {
      coordArray.push({
        x: measurement.time,
        y: measurement.temperature
      })
    });
    return coordArray;
  }
}
