import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SensorViewComponent } from './sensor-view/sensor-view.component';
import { HttpClientModule, HttpClient }    from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SensorViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
