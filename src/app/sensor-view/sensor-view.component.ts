import { Component, OnInit } from '@angular/core';
import { measurement} from 'src/measurement';
import * as CanvasJS from '../canvasjs.min.js';
import { MeasurementService } from '../measurement.service.js';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sensor-view',
  templateUrl: './sensor-view.component.html',
  styleUrls: ['./sensor-view.component.css']
})
export class SensorViewComponent implements OnInit {

  private mService: MeasurementService;
  private obSeriesData: Observable<Object>;
  lastMeasurement: measurement;
  constructor(private service: MeasurementService) {
    this.lastMeasurement = {
      deviceName: "none",
      pressure: 0,
      temperature: 0,
      time: null
    }
    this.mService = service;
    this.obSeriesData = this.mService.getMeasurements();
  }


  ngOnInit(): void {
    let interval = setInterval(() =>{
      this.updateLastMeasurement();
    }, 1000);
  }

  updateLastMeasurement(): void {
    let values = [];
    this.obSeriesData.subscribe(data =>{
      let values = data["results"][0]["series"][0]["values"][0];
      let deviceName = data["results"][0]["series"][0]["tags"]["name"];
      //values = values[values.length - 1];
      let output:measurement = {
        deviceName: deviceName,
        temperature: values[2],
        pressure: values[1],
        time: new Date(values[0]).toLocaleString()
      }
      this.lastMeasurement = output;
    });
  }

}
