export interface measurement 
{
    deviceName:String;
    temperature: Number;
    pressure:Number;
    time:String;
}